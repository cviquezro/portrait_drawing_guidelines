import os 
import argparse
from PIL import Image, ImageDraw
import numpy as np
import cv2
import face_recognition
import math
import sympy
from bokeh.plotting import figure, show, output_file, save 
from bokeh.models.glyphs import ImageURL
from bokeh.models import Plot
from bokeh.models.glyphs import Ellipse
from bokeh.models import LabelSet, Label, Title, ColumnDataSource

# Define some functions

def maxy(array):
    point_maxy = [[x,y] for [x,y] in array if\
                  abs(y) == (np.max(np.absolute(array), axis=0)[1])][0]
    return(point_maxy)

def miny(array):
    point_miny = [[x,y] for [x,y] in array if\
                  abs(y) == (np.min(np.absolute(array), axis=0)[1])][0]
    return(point_miny)


def find_ellipse_intersection(shifted_ellipse, point):
    
    """ In the shifted plane, 
        find the intersection between the ellipse and a given point"""
    
    evalp = lambda P: [float(P[0]), -float(P[1])]
    
    e = sympy.Ellipse((shifted_ellipse[0][0], 
                       -shifted_ellipse[0][1]), 
                       hradius=shifted_ellipse[1][0]/2, 
                       vradius=shifted_ellipse[1][1]/2)
    
    ln = sympy.Line(sympy.Point(x_lims[0], -point[1]), 
                    sympy.Point(x_lims[1], -point[1]))
    
    e_ln = e.intersection(ln)
    p1 = evalp(e_ln[0])
    p2 = evalp(e_ln[1])

    return(p1, p2)

    
def find_secondary_lines(shifted_landmarks_list):
    
    """This function finds the 4 secondary lines. 
       As input it requires the shifted landmarks. 
       It returns an array with two [x,y] points for each secondary line in the 
       original orientation"""
    
    shifted_points_dict = \
    {'bottom_mouth' : maxy(shifted_landmarks_list[0]['bottom_lip']),
     'mouth_nose' : list(np.mean([miny(shifted_landmarks_list[0]['top_lip']),
                                  maxy(shifted_landmarks_list[0]['nose_tip'])], axis=0)),
    'bottom_eyes' : maxy(shifted_landmarks_list[0]['eyes']),
    'top_eyes' : miny(shifted_landmarks_list[0]['brows'])}

    original_lines = []
    for key, value in shifted_points_dict.items():
        shifted_line = find_ellipse_intersection(shifted_ellipse, value)
        left = np.linalg.tensorsolve(rotation_matrix, shifted_line[0])
        right = np.linalg.tensorsolve(rotation_matrix, shifted_line[1])
        original_lines.append([[left[0], right[0]], [left[1], right[1]]])
    
    return  (original_lines)


def distance_inches(x, y):
    
    """Return distance in units of inches (assuming resolution of 72 pixels/inch)"""
    d = (np.sqrt((x[0]-x[1])**2 + (y[0]-y[1])**2))/72
    return(d)


def find_main_lines(shifted_ellipse):
    
    """Use the main axis of the shifted ellipse to find 
       axis of ellipse in original orientation """
    
    x_vertical_shifted = shifted_ellipse[0][0]
    x_horizontal_left_shifted = shifted_ellipse[0][0] - shifted_ellipse[1][0]/2
    x_horizontal_right_shifted = shifted_ellipse[0][0] + shifted_ellipse[1][0]/2
    
    y_vertical_top_shifted = shifted_ellipse[0][1] - shifted_ellipse[1][1]/2
    y_vertical_bottom_shifted = shifted_ellipse[0][1] + shifted_ellipse[1][1]/2
    y_horizontal_shifted = shifted_ellipse[0][1]
    
    vertical_top = np.linalg.tensorsolve(rotation_matrix, [x_vertical_shifted, y_vertical_top_shifted])
    vertical_bottom = np.linalg.tensorsolve(rotation_matrix, [x_vertical_shifted, y_vertical_bottom_shifted])
    
    horizontal_left = np.linalg.tensorsolve(rotation_matrix, [x_horizontal_left_shifted, y_horizontal_shifted])
    horizontal_right = np.linalg.tensorsolve(rotation_matrix, [x_horizontal_right_shifted, y_horizontal_shifted])
    
    return([[vertical_top[0], vertical_bottom[0]],
           [vertical_top[1], vertical_bottom[1]]],
           [[horizontal_left[0], horizontal_right[0]],
           [horizontal_left[1], horizontal_right[1]]])


def get_parallel_distances(plot, main_lines, hlines):

    """Find the distance separating secondary lines"""
    
    evalp = lambda P: [float(P[0]), float(P[1])]
    avg_x = []
    avg_y = []
    distances = []

    colors = ['#01665e', '#543005' , '#f6e8c3', '#bf812d', '#800000', '#80cdc1']
    
    main_lines_t = np.array(main_lines[0]).transpose()
    hpoint_top = sympy.Point(miny(main_lines_t))
    hpoint_bottom = sympy.Point(maxy(main_lines_t))
    horizontal_line = sympy.Line(hpoint_top, hpoint_bottom)

    line_points = [[hpoint_bottom]]
    sec_line = 0
    while sec_line < 4:
        p1 = sympy.Point((np.array(hlines[sec_line]).transpose())[0])
        p2 = sympy.Point((np.array(hlines[sec_line]).transpose())[1])
        line_points.append([p1, p2])
        sec_line += 1
        
    line_points.append(hpoint_top)

    i = 1
    while i < 4:
        
        l1 = sympy.Line(line_points[i][0], line_points[i][1])
        l2 = sympy.Line(line_points[i+1][0], line_points[i+1][1])        
        int_l1 = evalp(l1.intersection(horizontal_line)[0])
        int_l2 = evalp(l2.intersection(horizontal_line)[0])        
        xs = [int_l1[0], int_l2[0]]
        ys = [int_l1[1], int_l2[1]]
        p.line(xs, ys, color=colors[i], line_width=3)
        avg_x.append(np.mean(xs))
        avg_y.append(np.mean(ys))
        distances.append((distance_inches(xs, ys)).round(2))
        i += 1   
    
    outer_index = [-2]    
    for oi in outer_index:
        l_oi = sympy.Line(line_points[oi][0], line_points[oi][1])
        int_oi = evalp(l_oi.intersection(horizontal_line)[0])    
        xs_oi = [int_oi[0], float(hpoint_top[0])]
        ys_oi = [int_oi[1], float(hpoint_top[1])]
        p.line(xs_oi, ys_oi, color=colors[0], line_width=3,)
        avg_x.append(np.mean(xs_oi))
        avg_y.append(np.mean([ys_oi]))
        distances.append((distance_inches(xs_oi, ys_oi)).round(2))

    return(distances, avg_x, avg_y)

##################################

# Add parser to help read-in photos
parser = argparse.ArgumentParser()
parser.add_argument('-photo', action='store', dest='photo', type=str,
                    help='name image of portrait')
parser.add_argument('-out', action='store', dest='out', type=str,
                    help='name of output html file')

arguments = parser.parse_args()
if arguments.photo is not None:
    portrait = arguments.photo
else:
    portrait = input("Please indicate the file name of the portrait (ie 'filename.jpg'): ")

if arguments.out is not None:
    out_name = arguments.out
else:
    out_name = input("Please indicate the name you wish the html output to have (ie 'outname.html'): ")

# Read data and define dimensions of portrait
cwd_path = os.getcwd()
url = f'file://{cwd_path}/{portrait}'
img = cv2.imread(f'{portrait}')

width = img.shape[1]
height = img.shape[0]

max_width = int(math.ceil(width / 10.0)) * 10
max_height = int(math.ceil(height / 10.0)) * 10

x_lims = (-10, max_width)
y_lims = (max_height, -10)

# find landmarks using face_recognition
image = face_recognition.load_image_file('%s' %portrait)
face_locations = face_recognition.face_locations(image)
face_landmarks_list = face_recognition.face_landmarks(image)

# find main ellipse to describe face
face = np.array(face_landmarks_list[0]['chin'])
ellipse = cv2.fitEllipse(face)

angle = math.radians(-ellipse[2])  
rotation_matrix = np.array([[math.cos(angle), -math.sin(angle)], 
                            [math.sin(angle),  math.cos(angle)]])

# shift ellipse so that main axis of ellipse parallel to x and y
shifted_ellipse_center = rotation_matrix.dot(ellipse[0])
shifted_ellipse = (tuple(shifted_ellipse_center), ellipse[1], 0.0)

# shift landmarks to match orientation of shifted ellipse

shifted_landmarks = {}
for landmark in face_landmarks_list[0]:
    shifted_landmarks[landmark] = []
    for i in face_landmarks_list[0][landmark]:
        shifted_landmarks[landmark]\
        .append(rotation_matrix.dot(tuple(np.asarray(i))))

        
shifted_landmarks['eyes'] = np.concatenate((shifted_landmarks['left_eye'], 
                                           shifted_landmarks['right_eye']), axis=0)

shifted_landmarks['brows'] = np.concatenate((shifted_landmarks['left_eyebrow'], 
                                           shifted_landmarks['right_eyebrow']), axis=0)

shifted_landmarks_list = [shifted_landmarks]


# Code for plotting starts here

if x_lims[1] > 1500:
    fs = "20pt"
else:
    fs = "8pt"

p = figure(x_range=x_lims, y_range=y_lims, plot_width=width, plot_height=height,
           tools="pan,wheel_zoom,box_zoom,reset")

# plot photo 
p.image_url(url=[url], x=1, y=1, w=width, h=height, anchor='top_left')

# add ellipse
e0 = Ellipse(x=ellipse[0][0], y=ellipse[0][1], width=ellipse[1][0], height=ellipse[1][1],
                 angle=math.radians(-ellipse[2]), line_width=2, line_color='blue', fill_alpha=0,)
p.add_glyph(e0)

# add main lines 
main_lines = find_main_lines(shifted_ellipse)

p.line(main_lines[0][0], main_lines[0][1], color='black', line_width=3, 
       legend=("%.2f" %(distance_inches(main_lines[0][0], main_lines[0][1]))))

p.line(main_lines[1][0], main_lines[1][1], color='black', line_width=3, 
       legend=("%.2f" %(distance_inches(main_lines[1][0], main_lines[1][1]))))

# add secondary line
sec_color = 0
hlines = find_secondary_lines(shifted_landmarks_list)
for hline in hlines:
    p.line(hline[0], hline[1], color="red", line_width=3)
    sec_color += 1

d = get_parallel_distances(p, main_lines, hlines)

source = ColumnDataSource({"distances" : d[0],
                           "x" :  d[1],
                           "y" : d[2]})

labels = LabelSet(x="x", y="y", text="distances",
                  text_font_size=fs, text_color="red",
                  source=source, text_align='center')
p.add_layout(labels)

#### graph format
p.legend.location = "top_left"
p.legend.click_policy="hide"

output_file(f'{out_name}')
save(p)
