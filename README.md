# Automatic Guidelines for Drawing Portraits

Drawing portraits well requires reference lines for the novice artist. 
As someone who wants to learn how to draw faces, it was always frustrating to me how I had to choose between 
existing templates of stock images that had reference lines, and images that I really wanted to draw but that 
naturally did not come with reference lines. 

This little python script is the result of this frustration. It takes an image of a person's face and automatically draws 
reference lines to help an aspiring artist learn how to sketch the face.

To run it on the command line type:

```console
python3 portrait_guidelines.py -photo in_name.jpg -out out_name.html
```

where 'in_name.jpg' is the name of the portrait image and 'out_name.html' is 
the name of the output file (html) that will be saved in the current working directory 
(where the script and the in_name.jpg file should be).

The finished image should look a bit like the following (test image of me at a conference!): 

![alt text](https://gitlab.com/cviquezro/portrait_drawing_guidelines/raw/master/me_lines.png)


Dependencies (these are quite a few!):
- common: os, argparse, numpy, math, sympy
- less common: Pillow, opencv (this requires matplotlib), face_recognition (requires dlib), bokeh

